﻿# README #

### What is this repository for? ###

* Quick summary

PhpConsole gives PHP access to the Win32 OutputDebugString API method and allows PHP code running on windows to send output to debug tools as SysInternals DebugWnd (https://sysinternals.com) without interrupting execution.

* Version
2 

### How do I get set up? ###

* Summary of set up

1. Create your own key for signing this assembly.
2. Compile the project using Visual Studio.
3. Add your assembly to GAC (use "gacutil.exe")
4. Using PowerShell (Elevated) get the full assembly name using 
```POWERSHELL
[System.Reflection.AssemblyName]::GetAssemblyName("bin\Debug\PhpConsole.dll").FullName
```
5. Install the assembly into the GAC (Global Assembly Cache) so PHP can find it using:
```POWERSHELL
& "C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.8 Tools\gacutil.exe"  /i "bin\Debug\PhpConsole.dll"
```
Note: Depending on your setup the gacutil path migh be different.
Note: After each modification the assembly needs to be (re)added to the GAC.
6. Enabled the com_dotnet PHP extension in your PHP.ini by adding a line:
```ini
extension=php_com_dotnet.dll
```
* Configuration
The default message prefix is [ODS] which can be used for filtering purposes in DebugWnd.

* Dependencies

n/a.

* Database configuration

n/a.
* How to run tests

n/a.
* Deployment instructions
Add the following code to your PHP:
```PHP
    // Init $console
    //
    $console = new dotnet("PhpConsole, Version=1.0.14.0, Culture=neutral, PublicKeyToken=c4a0f801a32e3553", "PhpConsole.ConsoleUtils");

    // Use $console.
    //
    $console->ODS("Hello World from PhpConsole.");
```
Or better add it to a PHP class (the code will create dummy ODS function when com_php is not loaded):
```PHP
    // Init $this->console
    //
    if (extension_loaded('com_dotnet')) {
        // Load .Net Assembly fro GAC.
        $this->console = new dotnet("PhpConsole, Version=1.0.14.0, Culture=neutral, PublicKeyToken=c4a0f801a32e3553", "PhpConsole.ConsoleUtils");

    } else {
        // Generate a fake method ODS($msg) and Clear() if com_dotnet is not loaded.
        $this->console = new class() {
            public function ODS($msg) {
                $a=1;
            }
        };
        $this->console = new class() {
            public function Clear() {
                $a=1;
            }
        };
    }

    // Use PhpConsole
    //

    // Clear DebugView Window.
    //
    $this->console->Clear();
    $this->console->ODS("Hello World from PhpConsole.");
    $this->console->ODS(__METHOD__."() @ ". __LINE__);

    // Remove Prefix
    //
    $this->console->Prefix="";
    $this->console->ODS(__METHOD__."() @ ". __LINE__);

    // Change Prefix to '[PHP]'
    //
    $this->console->Prefix="PHP";
    $this->console->ODS(__METHOD__."() @ ". __LINE__);
```
### Contribution guidelines ###

* Please report bugs and propose fixes and new functionality without forking this repository.
