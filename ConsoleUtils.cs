﻿namespace PhpConsole
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// A console utilities.
    /// </summary>
    public class ConsoleUtils
    {
        /// <summary>
        /// The dbgviewclear magic string.
        /// </summary>
        private const string DBGVIEWCLEAR = "DBGVIEWCLEAR";

        /// <summary>
        /// The prefix.
        /// </summary>
        public String Prefix = "ODS";

        /// <summary>
        /// Output debug string.
        /// </summary>
        ///
        /// <param name="lpOutputString"> The output string. </param>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        static extern void OutputDebugString(string lpOutputString);

        /// <summary>
        /// OutputDebugString Access;
        /// </summary>
        ///
        /// <param name="msg"> The message. </param>
        public void ODS(string msg)
        {
            OutputDebugString($"{(String.IsNullOrEmpty(Prefix) ? String.Empty : $"[{Prefix}] ")}{msg}");
        }

        /// <summary>
        /// Clears this object to its blank/initial state.
        /// </summary>
#pragma warning disable CA1822 // Mark members as static
        public void Clear()
#pragma warning restore CA1822 // Mark members as static
        {
            OutputDebugString(DBGVIEWCLEAR);
        }
    }
}
